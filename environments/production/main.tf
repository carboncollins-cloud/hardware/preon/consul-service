terraform {
  backend "consul" {
    path = "terraform/preon-consul-service"
  }

  required_providers {
    consul = {
      source = "hashicorp/consul"
      version = "2.15.1"
    }

    vault = {
      source = "hashicorp/vault"
      version = "3.5.0"
    }
  }
}

provider "consul" {}
provider "vault" {}

resource "consul_node" "preon_nas" {
  name    = "preon"
  address = "192.168.20.202"

  meta = {
    "external-node"  = "true"
    "external-probe" = "true"
  }
}

resource "consul_service" "preon_https" {
  name = "preon"
  service_id = "preon-https"
  node = consul_node.preon_nas.name
  port = 5001
  tags = [
    "https",
    "traefik.enable=true",
    "traefik.http.routers.preon.entrypoints=https",
    "traefik.http.routers.preon.rule=Host(`preon.hq.carboncollins.se`)",
    "traefik.http.routers.preon.tls=true",
    "traefik.http.routers.preon.tls.certresolver=lets-encrypt",
    "traefik.http.routers.preon.tls.domains[0].main=*.hq.carboncollins.se",
    "traefik.http.services.preon.loadBalancer.serversTransport=insecureSkipVerify@file",
    "traefik.http.services.preon.loadbalancer.server.scheme=https"
  ]

  check {
    check_id = "httpsEndpoint"
    name = "Https health check"
    status = "warning"
    http = "https://${consul_node.preon_nas.address}:5001/"
    tls_skip_verify = true
    method = "HEAD"
    interval = "30s"
    timeout = "10s"
    deregister_critical_service_after = "48h"

    header {
      name  = "host"
      value = ["${consul_node.preon_nas.address}:5001"]
    }

    header {
      name  = "user-agent"
      value = ["consul-health-check"]
    }
  }
}

resource "consul_service" "preon_snmp" {
  name = "preon"
  service_id = "preon-snmp"
  node = consul_node.preon_nas.name
  port = 161
  tags = ["snmp"]

  check {
    check_id = "snmpEndpoint"
    name = "SNMP health check"
    status = "warning"
    tcp = "${consul_node.preon_nas.address}:161"
    interval = "15s"
    timeout = "5s"
    deregister_critical_service_after = "48h"
  }
}
